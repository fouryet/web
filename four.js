var moment = require('moment-timezone');
var _ = require('underscore');

function secondsFromFour(t)
{
  four = moment(t).startOf('day').add(16, 'h');

  return t.diff(four, 's')

}

function find()
{
  times = _.map(moment.tz.names(), function(tz) {
    return {
      timezone: tz,
      secondsFromFour: secondsFromFour(moment().tz(tz))
    }
  });

  fourZones = _.filter(times, function(t) {
    return t.secondsFromFour > 0 && t.secondsFromFour < 3600
  })

  fourZones = _.sortBy(fourZones, function(t) {
    return t.secondsFromFour
  })

  return _.pluck(fourZones, 'timezone');
}

module.exports = find
