/**
* Compute how many seconds a time is from 4pm. It will be negative if it's
* before 4, and positive otherwise.
*
* @param {moment} t Moment time object
* @return {moment} Duration in seconds past 4
*/
function secondsFromFour(t)
{
  four = moment(t).startOf('day').add(16, 'h');

  return t.diff(four, 's')

}

/**
* Compute a list of timezones where it's between 4pm and 5pm.
*
* @return {Array} List of timezone strings
*/
function fourTimezones()
{
  // get the time in all IANA timezones
  times = _.map(moment.tz.names(), function(tz) {
    return {
      timezone: tz,
      secondsFromFour: secondsFromFour(moment().tz(tz))
    }
  });

  // filter out those where it's not 4pm
  fourZones = _.filter(times, function(t) {
    return t.secondsFromFour > 0 && t.secondsFromFour < 3600
  })

  // sort by how far past 4 it is
  fourZones = _.sortBy(fourZones, function(t) {
    return t.secondsFromFour
  })

  // pluck out just the timezone names
  return _.pluck(fourZones, 'timezone');
}


/**
* Compute latitude and longitude information for a list of timezones
*
* @return {Array} List of objects with {lat, lng} for each timezone
*/
function locationsOfTimezones(tzs)
{
  console.log(tzs)

  // get the lat and long for each timezone
  var coords = _.map(tzs, function(place) {
    var loc = timezones['zones'][place];

    if (!loc)
      return {lat: 0, lng: 0}

    var pos = {
      lat: loc.lat,
      lng: loc.long
    };

    return pos
  });

  // remove the ones we don't know about
  coords = _.filter(coords, function(place) {
    return place.lng != 0 && place.lat != 0;
  })

  return coords;
}

/**
* Google Maps object.
*
* @property map
* @type {Object}
*/
var map;

/**
* Draw a circle on the global map object at a given lattitude and longitude
*
* @param {Object} coord Object with {lat, lng}
*/
function drawCircle(coord)
{
  var center = {lat: coord.lat, lng: coord.lng};

  var circle = new google.maps.Circle({
    center: center,
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 3,
    fillColor: '#FF0000',
    fillOpacity: 0.35,
    radius: 100000
  });

  circle.setMap(map)

  map.panTo(center);
}

/**
* Draw a polygon of the convex hull of a set of lattitude and longitude points
* on the global map object.
*
* @param {Array} coords Objects with {lat, lng}
*/
function drawConvexHull(coords)
{
  // compute the convex hull
  var convexHull = new ConvexHullGrahamScan();

  _.each(coords, function(p) {
    convexHull.addPoint(p.lng, p.lat);
  })

  var hullPoints = convexHull.getHull();

  // convert hull points to gmap points
  hullPoints = hullPoints.map(function (item) {
    return new google.maps.LatLng(item.y, item.x);
  });

  // create polygon for convex hull
  var poly = new google.maps.Polygon({
    paths: hullPoints,
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 3,
    fillColor: '#FF0000',
    fillOpacity: 0.35
  });

  // draw the polygon
  poly.setMap(map);

  // compute the center of the convex hull
  center = geolib.getCenter(
    _.map(hullPoints, function(point) {
      return {
        latitude: point.lat(),
        longitude: point.lng()
      };
    })
  );

  // pan the map to the center of the convex hull
  map.panTo(new google.maps.LatLng(center.latitude, center.longitude));
}


function draw(coords)
{
  // draw a circle if it's one place
  // otherwise, draw a polygon around the convex hull
  if (coords.length == 1)
    drawCircle(coords[0])
  else
    drawConvexHull(coords)
}

// create the map
map = new google.maps.Map(document.getElementById('map'), {
  zoom: 4,
  center: {lat: 0, lng: 0},
  mapTypeId: google.maps.MapTypeId.SATELLITE
});

// find the timezones where it's 4, get their lat / long and draw them
draw(locationsOfTimezones(fourTimezones()))
