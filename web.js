var express = require("express");
var four = require("./four.js")

var app = express();

var port = Number(process.env.PORT || 5000);
app.listen(port, function() {
  console.log("Listening on " + port);
});

app.use(express.static(__dirname + '/public'))

app.get('/api', function (req, res) {
  res.send(four());
});
